package ru.silverrid.bissness_logic_server.model;

import lombok.Getter;
import lombok.Setter;
/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Getter
@Setter
public class User {
	private String username;
	private String password;
	private String code;
}
