package ru.silverrid.bissness_logic_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BissnessLogicServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BissnessLogicServerApplication.class, args);
	}

}
