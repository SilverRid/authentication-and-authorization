package ru.silverrid.bissness_logic_server.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import ru.silverrid.bissness_logic_server.component.InitialAuthenticationFilter;
import ru.silverrid.bissness_logic_server.component.JwtAuthenticationFilter;
import ru.silverrid.bissness_logic_server.component.OtpAuthenticationProvider;
import ru.silverrid.bissness_logic_server.component.UsernamePasswordAuthenticationProvider;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Component
@RequiredArgsConstructor
public class SecurityConfiguration {

	private final InitialAuthenticationFilter initialAuthenticationFilter;
	private final JwtAuthenticationFilter jwtAuthenticationFilter;
	private final OtpAuthenticationProvider otpAuthenticationProvider;
	private final UsernamePasswordAuthenticationProvider usernamePasswordAuthenticationProvider;
	private final AuthenticationManager authenticationManager;

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.cors().disable();
		http.csrf().disable();
		http.authenticationManager(authenticationManager);
//				.authenticationProvider(otpAuthenticationProvider).authenticationProvider(usernamePasswordAuthenticationProvider);
		http.addFilterAt(initialAuthenticationFilter, BasicAuthenticationFilter.class).addFilterAfter(jwtAuthenticationFilter, BasicAuthenticationFilter.class);
		http.authorizeRequests().anyRequest().authenticated();
		return http.build();
	}
}
