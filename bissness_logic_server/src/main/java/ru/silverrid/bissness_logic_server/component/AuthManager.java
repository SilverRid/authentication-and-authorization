package ru.silverrid.bissness_logic_server.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Configuration
public class AuthManager {

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private UsernamePasswordAuthenticationProvider usernamePasswordAuthenticationProvider;
	@Autowired
	private OtpAuthenticationProvider otpAuthenticationProvider;
	@Bean
	public AuthenticationManager authenticationManager(HttpSecurity httpSecurity) throws Exception {
		return httpSecurity.getSharedObject(AuthenticationManagerBuilder.class).
				userDetailsService(userDetailsService).and()
				.authenticationProvider(otpAuthenticationProvider)
				.authenticationProvider(usernamePasswordAuthenticationProvider)
				.build();
	}
}
