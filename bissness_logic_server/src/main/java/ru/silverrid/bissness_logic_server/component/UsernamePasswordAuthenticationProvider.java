package ru.silverrid.bissness_logic_server.component;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import ru.silverrid.bissness_logic_server.service.UsernamePasswordAuthentication;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Component
@RequiredArgsConstructor
public class UsernamePasswordAuthenticationProvider implements AuthenticationProvider {
	private final AuthServerProxy proxy;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		String password = String.valueOf(authentication.getCredentials());
		proxy.sendAuth(username, password);
		return new UsernamePasswordAuthenticationToken(username, password);
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return UsernamePasswordAuthentication.class.isAssignableFrom(aClass);
	}
}
