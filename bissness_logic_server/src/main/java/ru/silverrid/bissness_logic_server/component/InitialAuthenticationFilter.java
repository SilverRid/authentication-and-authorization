package ru.silverrid.bissness_logic_server.component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.silverrid.bissness_logic_server.service.OtpAuthentication;
import ru.silverrid.bissness_logic_server.service.UsernamePasswordAuthentication;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Component
@RequiredArgsConstructor
public class InitialAuthenticationFilter extends OncePerRequestFilter {
	private final AuthenticationManager manager;

	@Value("${jwt.signing.key}")
	private String signingKey;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException {
		String username = request.getHeader("username");
		String password = request.getHeader("password");
		String code = request.getHeader("code");

		if (code == null) {
			Authentication a = new UsernamePasswordAuthentication(username, password);
			manager.authenticate(a);
		} else {
			Authentication a = new OtpAuthentication(username, code);
			manager.authenticate(a);
			SecretKey key = Keys.hmacShaKeyFor(signingKey.getBytes(StandardCharsets.UTF_8));
			String jwt = Jwts.builder()
					.setClaims(Map.of("username", username))
					.signWith(key)
					.compact();
			response.setHeader("Authorization", jwt);
		}
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) {
		return !request.getServletPath().equals("/login");
	}
}