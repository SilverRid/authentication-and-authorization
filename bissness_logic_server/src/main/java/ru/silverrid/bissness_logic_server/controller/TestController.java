package ru.silverrid.bissness_logic_server.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@RestController
public class TestController {
	@GetMapping("/test")
	public String test(Authentication authentication) {
		return "Hello, " + authentication.getName() + "!\n";
	}
}