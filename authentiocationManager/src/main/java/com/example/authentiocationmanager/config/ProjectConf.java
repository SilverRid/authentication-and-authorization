package com.example.authentiocationmanager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Configuration
public class ProjectConf {

//	@Bean
//	public UserDetailsService userDetailsService() {
//		UserDetails user = User.withUsername("bill")
//				.password("123")
//				.authorities("read")
//				.build();
//
//		return new InMemoryUserDetailsManager(user);
//	}
//
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return NoOpPasswordEncoder.getInstance();
//	}
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http, CustomAuthenticationProvider customAuthenticationProvider) throws Exception {
		http.authenticationProvider(customAuthenticationProvider).httpBasic();
		http.authorizeRequests().anyRequest().authenticated(); // required authentication
//		http.authorizeRequests().anyRequest().permitAll();      // permit all for every page
		return http.build();
	}


}
