package com.example.authentiocationmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthentiocationManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthentiocationManagerApplication.class, args);
	}

}
