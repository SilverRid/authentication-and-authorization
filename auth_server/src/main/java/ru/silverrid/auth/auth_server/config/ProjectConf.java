package ru.silverrid.auth.auth_server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Configuration
public class ProjectConf {

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http, CustomAuthenticationProvider customAuthenticationProvider) throws Exception {

//		http.authenticationProvider(customAuthenticationProvider)
//				.formLogin()
//				.successForwardUrl("/hello")
//				.failureForwardUrl("/login").permitAll();
////		http.authorizeRequests().anyRequest().permitAll(); // required authentication
//		http.authorizeRequests()
//				.antMatchers("/h2-console/**", "/login", "/login/**").permitAll()
////				.antMatchers("/**", "/").permitAll()
//				.antMatchers("/", "/hello").authenticated();
//
//		http.headers(headers -> headers.frameOptions().disable())
//				.csrf(AbstractHttpConfigurer::disable);
////				.csrf(csrf -> csrf
////						.ignoringAntMatchers("/h2-console/**"));// permit all for every page


		http.csrf().disable();
		http.cors().disable();
		http.headers(headers -> headers.frameOptions().disable())
				.csrf(AbstractHttpConfigurer::disable);
//				.csrf(csrf -> csrf
//						.ignoringAntMatchers("/h2-console/**"));// permit all for every page
		http.authorizeRequests().anyRequest().permitAll();

		return http.build();
	}
}
