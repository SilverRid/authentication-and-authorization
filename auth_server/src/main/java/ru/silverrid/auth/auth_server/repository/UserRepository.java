package ru.silverrid.auth.auth_server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.silverrid.auth.auth_server.model.User;

import java.util.Optional;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findUserByUsername(String userName);
}
