package ru.silverrid.auth.auth_server.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.silverrid.auth.auth_server.model.Otp;
import ru.silverrid.auth.auth_server.model.User;
import ru.silverrid.auth.auth_server.service.UserService;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@RestController
@RequiredArgsConstructor
public class AuthController {
	private final UserService userService;

	@PostMapping("/user/add")
	public void addUser(@RequestBody User user) {
		userService.addUser(user);
	}

	@PostMapping("/user/auth")
	public void auth(@RequestBody User user) {
		userService.auth(user);
	}

	@PostMapping("/otp/check")
	public void check(@RequestBody Otp otp, HttpServletResponse response) {
		if (userService.check(otp)) {
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}
	}
}
