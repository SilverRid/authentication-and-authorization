package ru.silverrid.auth.auth_server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Entity
@Getter
@Setter
public class Otp {
	@Id
	private String username;
	private String code;
}
