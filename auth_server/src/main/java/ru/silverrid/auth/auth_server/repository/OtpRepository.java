package ru.silverrid.auth.auth_server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.silverrid.auth.auth_server.model.Otp;

import java.util.Optional;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
public interface OtpRepository extends JpaRepository<Otp, String> {
	Optional<Otp> findOtpByUsername(String userName);
}
