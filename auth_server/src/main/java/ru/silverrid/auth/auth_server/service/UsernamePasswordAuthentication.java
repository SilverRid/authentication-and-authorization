package ru.silverrid.auth.auth_server.service;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
public class UsernamePasswordAuthentication extends UsernamePasswordAuthenticationToken {
	public UsernamePasswordAuthentication(Object principal, Object credentials) {
		super(principal, credentials);
	}

	public UsernamePasswordAuthentication(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
	}
}
