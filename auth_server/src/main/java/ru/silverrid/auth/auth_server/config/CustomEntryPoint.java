package ru.silverrid.auth.auth_server.config;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
public class CustomEntryPoint implements AuthenticationEntryPoint {
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
		response.addHeader("message", "IT'S FAULT BLT!");
		response.sendError(HttpStatus.UNAUTHORIZED.value());
	}
}
