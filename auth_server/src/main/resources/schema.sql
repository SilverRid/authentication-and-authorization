CREATE TABLE IF NOT EXISTS users
(
    id       INT         NOT NULL auto_increment,
    username VARCHAR(45) NOT NULL,
    password VARCHAR(145) NOT NULL,
    en boolean,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS authorities
(
    id       INT         NOT NULL auto_increment,
    username VARCHAR(45) NOT NULL,
    auth VARCHAR(45) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS otp (
    username VARCHAR(45) NOT NULL,
    code VARCHAR(45) NULL,
    PRIMARY KEY (username));