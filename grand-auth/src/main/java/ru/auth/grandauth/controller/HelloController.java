package ru.auth.grandauth.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.auth.grandauth.model.User;

import java.util.List;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@RestController
@RequiredArgsConstructor
public class HelloController {


	private final JdbcTemplate jdbcTemplate;
	@GetMapping("/hello")
	public String hello() {

		User user = jdbcTemplate.queryForObject("select username, password, en from users where username = ?", (rs, rowNum) -> {
			User user1 = new User();
			user1.setUsername(rs.getString("username"));
			user1.setPassword(rs.getString("password"));
			user1.setEnabled(rs.getBoolean("en"));
			return  user1;
		}, "bill");
		return user.toString();
	}
}
