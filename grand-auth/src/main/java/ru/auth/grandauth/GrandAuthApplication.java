package ru.auth.grandauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrandAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrandAuthApplication.class, args);
	}

}
