package ru.auth.grandauth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Configuration
public class ProjectConf {

//	@Bean
//	public UserDetailsService userDetailsService() {
//		UserDetails user = org.springframework.security.core.userdetails.User.withUserDetails(new User("bill","123","READ")).build();
//		UserDetails user2 = org.springframework.security.core.userdetails.User.withUserDetails(new User("john","321","READ")).build();
//
//		UserDetailManager userDetailManager = new InMemoryDetailService(List.of(user , user2));
//		return userDetailManager;
//	}

	/** default jdbc */
//	@Bean
//	public UserDetailsService userDetailsService(DataSource dataSource) {
//		return new JdbcUserDetailsManager(dataSource);
//	}

	/**
	 * custom jdbc
	 */
	@Bean
	public UserDetailsService userDetailsService(DataSource dataSource) {
		String usersByUsernameQuery = "select username, password, en from users where username = ?";
		String authsByUserQuery = "select username, auth from authorities where username = ?";
		var userDetailsManager = new JdbcUserDetailsManager(dataSource);
		userDetailsManager.setUsersByUsernameQuery(usersByUsernameQuery);
		userDetailsManager.setAuthoritiesByUsernameQuery(authsByUserQuery);
		return userDetailsManager;
	}

	/** NoOpPAssword */
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return NoOpPasswordEncoder.getInstance();
//	}
	/** choose encryption byDefault and choose encryption by string prefix String password ex.{noop}123 or {bcript}df233= */
	@Bean
	public PasswordEncoder passwordEncoder() {
		Map<String, PasswordEncoder> encoders = new HashMap<>();
		encoders.put("noop", NoOpPasswordEncoder.getInstance());
		encoders.put("bcrypt", new BCryptPasswordEncoder());
		encoders.put("scrypt", new SCryptPasswordEncoder());
		return new DelegatingPasswordEncoder("bcrypt", encoders);
	}

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.httpBasic();
//		http.authorizeRequests().anyRequest().authenticated(); // required authentication
		http.authorizeRequests()
				.antMatchers("/h2-console/**").permitAll()
				.antMatchers("/", "/hello").authenticated();

		http.headers(headers -> headers.frameOptions().disable())
				.csrf(csrf -> csrf
						.ignoringAntMatchers("/h2-console/**"));// permit all for every page
		return http.build();
	}
}
