INSERT INTO authorities (username, auth) VALUES ('bill', 'write');
INSERT INTO authorities (username, auth) VALUES ('olii', 'write');
INSERT INTO users (username, password, en) VALUES ('bill', '{noop}123', 1);
INSERT INTO users (username, password, en) VALUES ('olii', '{bcrypt}$2a$12$im.DUi2zgq/hTqlrRpvPvudiAbUPL.aEh70bD6lh.7tgEYh84lKra', 1);