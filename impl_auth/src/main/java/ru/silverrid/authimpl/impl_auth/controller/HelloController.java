package ru.silverrid.authimpl.impl_auth.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.concurrent.DelegatingSecurityContextCallable;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutorService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.silverrid.authimpl.impl_auth.model.User;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@RestController
@RequiredArgsConstructor
public class HelloController {

	@GetMapping("/hello")
	public String hello() {
		return "Hello!";
	}

	@GetMapping("/thread")
	public String thread() throws ExecutionException, InterruptedException {

		Callable<String> task = () -> {
			SecurityContext context = SecurityContextHolder.getContext();
			return context.getAuthentication().getName();
		};

		ExecutorService executorService = Executors.newCachedThreadPool();
		try {
			return "Hello, " + executorService.submit(new DelegatingSecurityContextCallable<>(task)).get() + "!";
		} finally {
			executorService.shutdown();
		}
	}

	@GetMapping("/decoratedThreadEecutor")
	public String decoratedThreadEecutor() throws ExecutionException, InterruptedException {

		Callable<String> task = () -> {
			SecurityContext context = SecurityContextHolder.getContext();
			return context.getAuthentication().getName();
		};

		ExecutorService executorService = Executors.newCachedThreadPool();
		DelegatingSecurityContextExecutorService delegatingSecurityContextExecutorService =
				new DelegatingSecurityContextExecutorService(executorService);
		try {
			return "delegating SCES Hello, " + delegatingSecurityContextExecutorService.submit(task).get() + "!";
		} finally {
			executorService.shutdown();
		}
	}

	@GetMapping("/sec")
	public String getFromSecurityContext(Authentication authentication) {
		StringBuilder str = new StringBuilder();

		str.append(authentication.getPrincipal())
				.append(" \n pass: ")
				.append(authentication.getCredentials())
				.append(" \n  auths: ")
				.append(authentication.getAuthorities())
				.append(" \n details: ")
				.append(authentication.getDetails())
		;
		return str.toString();
	}
}
