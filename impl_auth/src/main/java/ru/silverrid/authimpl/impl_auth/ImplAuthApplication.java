package ru.silverrid.authimpl.impl_auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImplAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImplAuthApplication.class, args);
	}

}
