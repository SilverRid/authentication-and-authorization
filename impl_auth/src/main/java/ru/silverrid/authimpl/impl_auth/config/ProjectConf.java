package ru.silverrid.authimpl.impl_auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractAuthenticationFilterConfigurer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Configuration
public class ProjectConf {

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http, CustomAuthenticationProvider customAuthenticationProvider) throws Exception {

//		http.authenticationProvider(customAuthenticationProvider)
//				.formLogin()
//				.successForwardUrl("/hello")
//				.failureForwardUrl("/login").permitAll();
////		http.authorizeRequests().anyRequest().permitAll(); // required authentication
//		http.authorizeRequests()
//				.antMatchers("/h2-console/**", "/login", "/login/**").permitAll()
////				.antMatchers("/**", "/").permitAll()
//				.antMatchers("/", "/hello").authenticated();
//
//		http.headers(headers -> headers.frameOptions().disable())
//				.csrf(AbstractHttpConfigurer::disable);
////				.csrf(csrf -> csrf
////						.ignoringAntMatchers("/h2-console/**"));// permit all for every page


		http.authorizeRequests(authorizeRequests -> authorizeRequests.anyRequest()
				.authenticated())
				.httpBasic(c -> {c.realmName("SUPER");
				c.authenticationEntryPoint(new CustomEntryPoint());})
				.formLogin(defaults -> {})
				.csrf(AbstractHttpConfigurer::disable);
		return http.build();
	}
}
